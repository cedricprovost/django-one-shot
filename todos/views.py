from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    task = get_object_or_404(TodoList, id=id)
    context = {
        "todo_items": task,
    }
    return render(request, "todos/detail.html", context)
